package com.oosd.finalproject.travelexpertsapp.DAO;

import com.oosd.finalproject.travelexpertsapp.model.Customer;

public interface CustomerAsyncResonponse {
    void getCustomerProcessFinished(Customer customerOut);
    void updateCustomerProcessFinished(Customer updatedCustomerOut);
}
