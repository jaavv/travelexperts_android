package com.oosd.finalproject.travelexpertsapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.oosd.finalproject.travelexpertsapp.R;
import com.oosd.finalproject.travelexpertsapp.model.Booking;
import com.oosd.finalproject.travelexpertsapp.model.Package;

import java.util.List;

public class ManageTripsRecycleViewAdapter extends RecyclerView.Adapter<ManageTripsRecycleViewAdapter.CustomViewHolder> implements View.OnClickListener {

    private List<Booking> bookingList;

    public ManageTripsRecycleViewAdapter(List<Booking> bookingList) {
        this.bookingList = bookingList;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        public final View myView;

        TextView tvBookingNo;
//        TextView tvBookingDate;

        CustomViewHolder(View itemView) {
            super(itemView);
            myView = itemView;

            tvBookingNo = myView.findViewById(R.id.tvBookingNo);
//            tvBookingDate = myView.findViewById(R.id.tvBookingDate);
        }

    }

    @Override
    public ManageTripsRecycleViewAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_bookings, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ManageTripsRecycleViewAdapter.CustomViewHolder holder, int position) {
        holder.tvBookingNo.setText(bookingList.get(position).getBookingNo());
//        holder.tvBookingDate.setText(String.valueOf(bookingList.get(position).getBookingDate()));
    }

    @Override
    public int getItemCount() {
        return bookingList.size();
    }

    @Override
    public void onClick(View view) {

    }
}
