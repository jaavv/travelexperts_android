package com.oosd.finalproject.travelexpertsapp.api;

import com.oosd.finalproject.travelexpertsapp.model.CustomerAuth;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface GerritAPI {

    @POST("login")
    Call<CustomerAuth> postAuthentication(@Body CustomerAuth customerAuth);
}
