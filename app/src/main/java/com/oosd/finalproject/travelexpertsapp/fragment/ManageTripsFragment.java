package com.oosd.finalproject.travelexpertsapp.fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.oosd.finalproject.travelexpertsapp.DAO.*;
import com.oosd.finalproject.travelexpertsapp.R;
import com.oosd.finalproject.travelexpertsapp.activity.MainActivity;
import com.oosd.finalproject.travelexpertsapp.adapter.ManageTripsRecycleViewAdapter;
import com.oosd.finalproject.travelexpertsapp.adapter.PackageRecycleViewAdapter;
import com.oosd.finalproject.travelexpertsapp.api.RetrofitClient;
import com.oosd.finalproject.travelexpertsapp.model.Booking;
import com.oosd.finalproject.travelexpertsapp.model.Package;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.util.ArrayList;
import java.util.List;

public class ManageTripsFragment extends Fragment implements BookingsAsyncResponse {

    private int customerID;

    private BookingDAO bookingDAO;
    private List<Booking> booking;

    private static final String CUSTOMER_ID = "customerId";

    ManageTripsRecycleViewAdapter manageTripsRecycleViewAdapter;
    RecyclerView recyclerView;
    View rootView;
    List<Booking> bookingList;

    public static ManageTripsFragment newInstance(int customerId) {
        ManageTripsFragment fragment = new ManageTripsFragment();
        Bundle args = new Bundle();
        args.putInt(CUSTOMER_ID, customerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        bookingList = new ArrayList<>();
        rootView = inflater.inflate(R.layout.fragment_manage_trips, container, false);

        recyclerView = rootView.findViewById(R.id.manage_trips_recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ManageTripsDAO trips = RetrofitClient.getRetrofitInstance().create(ManageTripsDAO.class);
        Call<List<Booking>> call = trips.getBookingById(customerID);
        call.enqueue(new Callback<List<Booking>>() {
            @Override
            public void onResponse(Call<List<Booking>> call, Response<List<Booking>> response) {
                bookingList = response.body();
                loadDataList(bookingList);
            }

            @Override
            public void onFailure(Call<List<Booking>> call, Throwable t) {
                Toast.makeText(getContext(), "Unable to load booking history", Toast.LENGTH_LONG).show();
            }
        });
        return rootView;
    }

    private void loadDataList(List<Booking> bookingList) {
        manageTripsRecycleViewAdapter = new ManageTripsRecycleViewAdapter(bookingList);
        recyclerView.setAdapter(manageTripsRecycleViewAdapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Manage Trips");
        if (getArguments() != null) {
            this.customerID = getArguments().getInt(CUSTOMER_ID);
            Toast.makeText(getContext(), String.valueOf(this.customerID), Toast.LENGTH_LONG).show();
            bookingDAO = new BookingDAO();
            bookingDAO.delegate = this;

            bookingDAO.getBookingByCustId(this.customerID, getContext());
        }
    }

    @Override
    public void processFinished(List<Booking> bookingOut) {
        booking = bookingOut;
        loadDataList(booking);
    }
}





//public class ManageTripsFragment extends Fragment implements BookingsAsyncResponse {
//
//    ManageTripsRecycleViewAdapter manageTripsRecycleViewAdapter;
//    RecyclerView recyclerView;
//    View rootView;
//    List<Booking> bookingList;
//    private static final String CUSTOMER_ID = "customerId";
//    private int customerId;
//    public ManageTripsFragment()
//    {
//        //Empty constructor required by java
//    }
//    /*
//    public static ProfileFragment newInstance(int customerId) {
//        ProfileFragment fragment = new ProfileFragment();
//        Bundle args = new Bundle();
//        args.putInt(CUSTOMER_ID, customerId);
//        fragment.setArguments(args);
//        return fragment;
//    }
//    * */
//    public static ManageTripsFragment newInstane(int custId)
//    {
//        ManageTripsFragment fragment = new ManageTripsFragment();
//        Bundle args = new Bundle();
//        args.putInt(CUSTOMER_ID, custId);
//        fragment.setArguments(args);
//        return fragment;
//    }
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        bookingList = new ArrayList<>();
//        rootView = inflater.inflate(R.layout.fragment_manage_trips, container, false);
//        recyclerView = rootView.findViewById(R.id.manage_trips_recycler_view);
//
//       /* PackageDAO packages = RetrofitClient.getRetrofitInstance().create(PackageDAO.class);
//        Call<List<Package>> call = packages.getAllPackages();
//        call.enqueue(new Callback<List<Package>>() {
//            @Override
//            public void onResponse(Call<List<Package>> call, Response<List<Package>> response) {
//                // packageList retrives data but I cant show it back to the adapter
//                packageList = response.body();
//            }
//            @Override
//            public void onFailure(Call<List<Package>> call, Throwable t) {
//                Toast.makeText(getContext(), "Unable to load packages", Toast.LENGTH_LONG).show();
//            }
//        });*/
//        return rootView;
//    }
//    private void loadDataList(List<Booking> bookingList) {
//        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
//        manageTripsRecycleViewAdapter = new ManageTripsRecycleViewAdapter(bookingList);
//        recyclerView.setAdapter(manageTripsRecycleViewAdapter);
////        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
//    }
//    @Override
//    public void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        if (getArguments() != null) {
//            customerId = getArguments().getInt(CUSTOMER_ID);
//            //Toast.makeText(this.getContext() , "The customer ID is " + customerId, Toast.LENGTH_LONG).show();
//            BookingDAO bookingDAO = new BookingDAO();
//            bookingDAO.delegate = this;
//            bookingDAO.getBookingByCustId(customerId, getContext());
//            //customerDAO.getCustomerById(customerId, getContext());
//        }
//    }
//    @Override
//    public void processFinished(List<Booking> bookingOut) {
//        loadDataList(bookingOut);
//    }
//}