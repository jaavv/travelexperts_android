package com.oosd.finalproject.travelexpertsapp.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.oosd.finalproject.travelexpertsapp.DAO.PackageDAO;
import com.oosd.finalproject.travelexpertsapp.R;
import com.oosd.finalproject.travelexpertsapp.adapter.PackageRecycleViewAdapter;
import com.oosd.finalproject.travelexpertsapp.api.RetrofitClient;
import com.oosd.finalproject.travelexpertsapp.model.Package;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class VacationsFragment extends Fragment {

    PackageRecycleViewAdapter packageRecycleViewAdapter;
    RecyclerView recyclerView;
    View rootView;
    List<Package> packageList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        packageList = new ArrayList<>();
        rootView = inflater.inflate(R.layout.fragment_vacations, container, false);

        recyclerView = rootView.findViewById(R.id.package_recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        PackageDAO packages = RetrofitClient.getRetrofitInstance().create(PackageDAO.class);
        Call<List<Package>> call = packages.getAllPackages();
        call.enqueue(new Callback<List<Package>>() {
            @Override
            public void onResponse(Call<List<Package>> call, Response<List<Package>> response) {
                packageList = response.body();
                loadDataList(packageList);
            }

            @Override
            public void onFailure(Call<List<Package>> call, Throwable t) {
                Toast.makeText(getContext(), "Unable to load packages", Toast.LENGTH_LONG).show();
            }
        });
        return rootView;
    }

    private void loadDataList(List<Package> packageList) {
        packageRecycleViewAdapter = new PackageRecycleViewAdapter(packageList);
        recyclerView.setAdapter(packageRecycleViewAdapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Vacation Packages");
    }
}
