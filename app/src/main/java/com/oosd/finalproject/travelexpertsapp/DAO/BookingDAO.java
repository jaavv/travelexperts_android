package com.oosd.finalproject.travelexpertsapp.DAO;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oosd.finalproject.travelexpertsapp.api.APIUtils;
import com.oosd.finalproject.travelexpertsapp.model.Booking;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

public class BookingDAO {

    // http://localhost:8080/TravelExperts/webapi/customers/104/bookings
    private static final String BOOKING_SERVICE_URL = APIUtils.BASE_URL + "customers/";
    //{customerID}/bookings/

    private static String getBookingURL = "";
    StringBuffer buffer = new StringBuffer();
    public BookingsAsyncResponse delegate;


    public void getBookingByCustId(int customerId, Context context)
    {

        getBookingURL = BOOKING_SERVICE_URL + customerId + "/bookings";

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getBookingURL,
                null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        Gson gson = new Gson();
                        Type listType = new TypeToken<List<Booking>>(){}.getType();
                        // Booking booking = gson.fromJson(response.toString(), Booking.class);
                        List<Booking> bookings = gson.fromJson(response.toString(), listType);
                        delegate.processFinished(bookings);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {

                    }
                });
        requestQueue.add(jsonObjectRequest);
    }
}