package com.oosd.finalproject.travelexpertsapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BookingDetail {

    @SerializedName("agencyCommission")
    @Expose
    private double agencyCommission;
    @SerializedName("basePrice")
    @Expose
    private double basePrice;
    @SerializedName("bookingDetailId")
    @Expose
    private int bookingDetailId;
    @SerializedName("bookingId")
    @Expose
    private int bookingId;
    @SerializedName("classId")
    @Expose
    private String classId;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("feeId")
    @Expose
    private String feeId;
    @SerializedName("itineraryNo")
    @Expose
    private double itineraryNo;
    @SerializedName("productSupplierId")
    @Expose
    private int productSupplierId;
    @SerializedName("regionId")
    @Expose
    private String regionId;
    @SerializedName("tripEnd")
    @Expose
    private String tripEnd;
    @SerializedName("tripStart")
    @Expose
    private String tripStart;

    public double getAgencyCommission() {
        return agencyCommission;
    }

    public void setAgencyCommission(double agencyCommission) {
        this.agencyCommission = agencyCommission;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public int getBookingDetailId() {
        return bookingDetailId;
    }

    public void setBookingDetailId(int bookingDetailId) {
        this.bookingDetailId = bookingDetailId;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFeeId() {
        return feeId;
    }

    public void setFeeId(String feeId) {
        this.feeId = feeId;
    }

    public double getItineraryNo() {
        return itineraryNo;
    }

    public void setItineraryNo(double itineraryNo) {
        this.itineraryNo = itineraryNo;
    }

    public int getProductSupplierId() {
        return productSupplierId;
    }

    public void setProductSupplierId(int productSupplierId) {
        this.productSupplierId = productSupplierId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getTripEnd() {
        return tripEnd;
    }

    public void setTripEnd(String tripEnd) {
        this.tripEnd = tripEnd;
    }

    public String getTripStart() {
        return tripStart;
    }

    public void setTripStart(String tripStart) {
        this.tripStart = tripStart;
    }

}