package com.oosd.finalproject.travelexpertsapp.DAO;

import com.oosd.finalproject.travelexpertsapp.model.Booking;

import java.util.List;

public interface BookingsAsyncResponse {
    void processFinished(List<Booking> bookingOut);
}
