package com.oosd.finalproject.travelexpertsapp.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.oosd.finalproject.travelexpertsapp.DAO.PackageDAO;
import com.oosd.finalproject.travelexpertsapp.R;
import com.oosd.finalproject.travelexpertsapp.adapter.PackageRecycleViewAdapter;
import com.oosd.finalproject.travelexpertsapp.api.RetrofitClient;
import com.oosd.finalproject.travelexpertsapp.fragment.*;
import com.oosd.finalproject.travelexpertsapp.model.Package;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.util.List;

/**
 *
 * OOSD SPRING 2018 FINAL PROJECT
 * @author Marc Javier
 * @author Prince Nimoh
 * @author Safiq Momin
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;

    private int customerID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //get the intent
        Intent intent = getIntent();

        //Get data from the intent
        customerID = intent.getIntExtra("CUSTOMER_ID", 0);

        Toolbar toolbar = findViewById(R.id.toolbar);


        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(botNavListener);

        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        /**
         *
         * first fragment to launch on start
         * 'if statement' prevents onCreate trigger when screen is rotated
         */
        if (savedInstanceState == null) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                new ExploreFragment()).commit();
        navigationView.setCheckedItem(R.id.nav_explore);
        }

        //Get profile icon and add listener
        ImageButton imageBtnProfile = findViewById(R.id.imageBtnProfile);

        imageBtnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext(), "Profile clicked", Toast.LENGTH_LONG).show();

                //int customerId = 104;

                //Get an instance of the profile fragment
                ProfileFragment profileFragment = ProfileFragment.newInstance(customerID);
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                //Display the transaction
                fragmentTransaction.add(R.id.fragment_container, profileFragment).addToBackStack(null).commit();
                //fragmentTransaction.replace(R.id.fragment_container, profileFragment).commit();

            }
        });
    }

    /**
     *
     * Bottom Navigation fragment toggle
     */
    private BottomNavigationView.OnNavigationItemSelectedListener botNavListener =
        new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment selectedFragment = null;
                switch (item.getItemId()) {
                    case R.id.nav_explore:
                        selectedFragment = new ExploreFragment();
                        break;
                    case R.id.nav_book:
                        selectedFragment = new BookFragment();
                        break;
                    case R.id.nav_flight_status:
                        selectedFragment = new FlightStatusFragment();
                        break;
                }

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                        selectedFragment).commit();
                return true;
            }
        };



    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /**
     *
     * Navigation drawer fragment toggle
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment selectedFragment = null;

        switch (item.getItemId()) {
            case R.id.nav_vacations:
                selectedFragment = new VacationsFragment();
                break;
            case R.id.nav_checkin:
                selectedFragment = new CheckInFragment();
                break;
            case R.id.nav_manage_trips:
                selectedFragment = ManageTripsFragment.newInstance(customerID);
                break;
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                selectedFragment).commit();
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public int getCustID() {
        return customerID;
    }
}
