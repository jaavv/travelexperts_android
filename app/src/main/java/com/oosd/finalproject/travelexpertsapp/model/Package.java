package com.oosd.finalproject.travelexpertsapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Package {

    @SerializedName("packageId")
    @Expose
    private int packageId;
    @SerializedName("pkgAgencyCommission")
    @Expose
    private double pkgAgencyCommission;
    @SerializedName("pkgBasePrice")
    @Expose
    private double pkgBasePrice;
    @SerializedName("pkgDesc")
    @Expose
    private String pkgDesc;
    @SerializedName("pkgEndDate")
    @Expose
    private String pkgEndDate;
    @SerializedName("pkgName")
    @Expose
    private String pkgName;
    @SerializedName("pkgStartDate")
    @Expose
    private String pkgStartDate;

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public double getPkgAgencyCommission() {
        return pkgAgencyCommission;
    }

    public void setPkgAgencyCommission(double pkgAgencyCommission) {
        this.pkgAgencyCommission = pkgAgencyCommission;
    }

    public double getPkgBasePrice() {
        return pkgBasePrice;
    }

    public void setPkgBasePrice(double pkgBasePrice) {
        this.pkgBasePrice = pkgBasePrice;
    }

    public String getPkgDesc() {
        return pkgDesc;
    }

    public void setPkgDesc(String pkgDesc) {
        this.pkgDesc = pkgDesc;
    }

    public String getPkgEndDate() {
        return pkgEndDate;
    }

    public void setPkgEndDate(String pkgEndDate) {
        this.pkgEndDate = pkgEndDate;
    }

    public String getPkgName() {
        return pkgName;
    }

    public void setPkgName(String pkgName) {
        this.pkgName = pkgName;
    }

    public String getPkgStartDate() {
        return pkgStartDate;
    }

    public void setPkgStartDate(String pkgStartDate) {
        this.pkgStartDate = pkgStartDate;
    }

}
