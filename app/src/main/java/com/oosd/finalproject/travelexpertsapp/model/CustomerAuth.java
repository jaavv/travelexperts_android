package com.oosd.finalproject.travelexpertsapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerAuth {

    @SerializedName("customerId")
    @Expose
    private int customerId;
    @SerializedName("customerEmail")
    @Expose
    private String customerEmail;
    @SerializedName("customerPassword")
    @Expose
    private String customerPassword;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPassword() {
        return customerPassword;
    }

    public void setCustomerPassword(String customerPassword) {
        this.customerPassword = customerPassword;
    }

}
