package com.oosd.finalproject.travelexpertsapp.DAO;

import com.oosd.finalproject.travelexpertsapp.api.APIUtils;
import com.oosd.finalproject.travelexpertsapp.model.Booking;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

public interface ManageTripsDAO {

    String BASE_URL = APIUtils.BASE_URL;

    @GET("http://35.182.90.133:8080/TravelExperts/webapi/customers/{customerId}/bookings")
    Call<List<Booking>> getBookingById(@Path("customerId") int customerId);
}
