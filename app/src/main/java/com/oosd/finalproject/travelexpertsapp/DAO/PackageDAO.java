package com.oosd.finalproject.travelexpertsapp.DAO;

import com.oosd.finalproject.travelexpertsapp.api.APIUtils;
import com.oosd.finalproject.travelexpertsapp.model.Package;
import retrofit2.Call;
import retrofit2.http.GET;

import java.util.List;

public interface PackageDAO {

    String BASE_URL = APIUtils.BASE_URL + "packages";

    @GET(BASE_URL)
    Call<List<Package>> getAllPackages();

}
