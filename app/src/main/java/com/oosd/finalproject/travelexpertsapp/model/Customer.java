package com.oosd.finalproject.travelexpertsapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer {

    @SerializedName("agentId")
    @Expose
    private int agentId;
    @SerializedName("custAddress")
    @Expose
    private String custAddress;
    @SerializedName("custBusPhone")
    @Expose
    private String custBusPhone;
    @SerializedName("custCity")
    @Expose
    private String custCity;
    @SerializedName("custCountry")
    @Expose
    private String custCountry;
    @SerializedName("custEmail")
    @Expose
    private String custEmail;
    @SerializedName("custFirstName")
    @Expose
    private String custFirstName;
    @SerializedName("custHomePhone")
    @Expose
    private String custHomePhone;
    @SerializedName("custLastName")
    @Expose
    private String custLastName;
    @SerializedName("custPostal")
    @Expose
    private String custPostal;
    @SerializedName("custProv")
    @Expose
    private String custProv;
    @SerializedName("customerId")
    @Expose
    private int customerId;

    public int getAgentId() {
        return agentId;
    }

    public void setAgentId(int agentId) {
        this.agentId = agentId;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustBusPhone() {
        return custBusPhone;
    }

    public void setCustBusPhone(String custBusPhone) {
        this.custBusPhone = custBusPhone;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getCustCountry() {
        return custCountry;
    }

    public void setCustCountry(String custCountry) {
        this.custCountry = custCountry;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public String getCustFirstName() {
        return custFirstName;
    }

    public void setCustFirstName(String custFirstName) {
        this.custFirstName = custFirstName;
    }

    public String getCustHomePhone() {
        return custHomePhone;
    }

    public void setCustHomePhone(String custHomePhone) {
        this.custHomePhone = custHomePhone;
    }

    public String getCustLastName() {
        return custLastName;
    }

    public void setCustLastName(String custLastName) {
        this.custLastName = custLastName;
    }

    public String getCustPostal() {
        return custPostal;
    }

    public void setCustPostal(String custPostal) {
        this.custPostal = custPostal;
    }

    public String getCustProv() {
        return custProv;
    }

    public void setCustProv(String custProv) {
        this.custProv = custProv;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }
}
