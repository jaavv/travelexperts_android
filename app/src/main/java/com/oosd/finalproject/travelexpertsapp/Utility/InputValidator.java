package com.oosd.finalproject.travelexpertsapp.Utility;

import android.text.TextUtils;
import android.widget.EditText;

import com.oosd.finalproject.travelexpertsapp.R;

import java.util.regex.Pattern;

public class InputValidator {


    public static boolean isValidPhoneNumber(EditText editText)
    {
        boolean valid = true;

        String phoneNumber  = editText.getText().toString();

        String regex = "\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}";
        Pattern pattern = Pattern.compile(regex);
       if(!pattern.matcher(phoneNumber).matches())
        {
            editText.setError("Please provide a 10 digit phone number");

        }
        else
        {
            valid = true;
        }
        return valid;
    }

    public static boolean isValidPostalCode(EditText editText)
    {
        boolean valid = false;
        String postalCode = editText.getText().toString();

        String regex = "^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$";
        Pattern pattern = Pattern.compile(regex);
        if(TextUtils.isEmpty(postalCode))
        {
            editText.setError("Postal code is a required field");

        }
        else if(!pattern.matcher(postalCode).matches())
        {
            editText.setError("Postal code must be of the format T2Y 4B6");

        }
        else
        {
            valid = true;
        }

        return valid;
    }

    public static boolean isValidEmail(EditText editText)
    {
        boolean valid = true;

        /*
        String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);

        if (TextUtils.isEmpty(email)) {
            editText.setError("Email is a required field");

        } else if (!pattern.matcher(email).matches()) {
            editText.setError("In valid email");

        }
        else
        {
            valid = true;
        }*/

        return valid;
    }
}
