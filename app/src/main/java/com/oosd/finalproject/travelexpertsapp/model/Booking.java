package com.oosd.finalproject.travelexpertsapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Booking {

    @SerializedName("bookingDate")
    @Expose
    private String bookingDate;
    @SerializedName("bookingId")
    @Expose
    private int bookingId;
    @SerializedName("bookingNo")
    @Expose
    private String bookingNo;
    @SerializedName("customerId")
    @Expose
    private int customerId;
    @SerializedName("packageId")
    @Expose
    private int packageId;
    @SerializedName("travelerCount")
    @Expose
    private double travelerCount;
    @SerializedName("tripTypeId")
    @Expose
    private String tripTypeId;

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public int getBookingId() {
        return bookingId;
    }

    public void setBookingId(int bookingId) {
        this.bookingId = bookingId;
    }

    public String getBookingNo() {
        return bookingNo;
    }

    public void setBookingNo(String bookingNo) {
        this.bookingNo = bookingNo;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getPackageId() {
        return packageId;
    }

    public void setPackageId(int packageId) {
        this.packageId = packageId;
    }

    public double getTravelerCount() {
        return travelerCount;
    }

    public void setTravelerCount(double travelerCount) {
        this.travelerCount = travelerCount;
    }

    public String getTripTypeId() {
        return tripTypeId;
    }

    public void setTripTypeId(String tripTypeId) {
        this.tripTypeId = tripTypeId;
    }
}