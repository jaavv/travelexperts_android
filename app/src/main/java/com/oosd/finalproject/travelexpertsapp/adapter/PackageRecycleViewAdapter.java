package com.oosd.finalproject.travelexpertsapp.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.oosd.finalproject.travelexpertsapp.R;
import com.oosd.finalproject.travelexpertsapp.activity.MainActivity;
import com.oosd.finalproject.travelexpertsapp.model.Package;

import java.util.List;

public class PackageRecycleViewAdapter extends RecyclerView.Adapter<PackageRecycleViewAdapter.CustomViewHolder> implements View.OnClickListener {


    private List<Package> packages;

    Context context;

    public PackageRecycleViewAdapter(List<Package> packages) {
        this.packages = packages;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        public final View myView;

        TextView tvPackageName;
//        TextView tvPackagePrice;
        ImageView ivPackagePicture;

        CustomViewHolder(View itemView) {
            super(itemView);
            myView = itemView;

            tvPackageName = myView.findViewById(R.id.tvPackageName);
//            tvPackagePrice = myView.findViewById(R.id.tvPackagePrice);
            ivPackagePicture = myView.findViewById(R.id.placeImage);
        }

    }

    @Override
    public PackageRecycleViewAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_packages, parent, false);
        return new CustomViewHolder(view);
    }



    @Override
    public void onBindViewHolder(PackageRecycleViewAdapter.CustomViewHolder holder, int position) {

        int[] images = new int[4];
        images[0] = R.drawable.package_image1;
        images[1] = R.drawable.package_image2;
        images[2] = R.drawable.package_image3;
        images[3] = R.drawable.package_image4;


        holder.tvPackageName.setText(packages.get(position).getPkgName());
//        holder.tvPackagePrice.setText(String.valueOf(packages.get(position).getPkgBasePrice()));

        if (position < images.length) {
            holder.ivPackagePicture.setImageResource(images[position]);
        } else {
            holder.ivPackagePicture.setImageResource(R.drawable.package_image0);
        }

    }


    @Override
    public int getItemCount() {
        return packages.size();
    }

    @Override
    public void onClick(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context.getApplicationContext());
        builder.setTitle("Test").setMessage("Testing").show();
    }
}
