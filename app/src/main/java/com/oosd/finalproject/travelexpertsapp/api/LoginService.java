package com.oosd.finalproject.travelexpertsapp.api;

import com.oosd.finalproject.travelexpertsapp.model.Customer;
import com.oosd.finalproject.travelexpertsapp.model.CustomerAuth;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface LoginService {

    @POST
    ("http://35.182.90.133:8080/TravelExperts/webapi/login")
    Call<Customer> getCustomer(@Header("Authorization") String authHeader);
}
