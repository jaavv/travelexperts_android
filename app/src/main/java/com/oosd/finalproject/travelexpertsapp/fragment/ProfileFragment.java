package com.oosd.finalproject.travelexpertsapp.fragment;

import android.os.Bundle;
//import android.app.Fragment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.oosd.finalproject.travelexpertsapp.DAO.CustomerAsyncResonponse;
import com.oosd.finalproject.travelexpertsapp.DAO.CustomerDAO;
import com.oosd.finalproject.travelexpertsapp.R;
import com.oosd.finalproject.travelexpertsapp.Utility.InputValidator;
import com.oosd.finalproject.travelexpertsapp.model.Customer;

import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment implements CustomerAsyncResonponse {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String CUSTOMER_ID = "customerId";
    private CustomerDAO customerDAO;
    private Customer customer;


    // TODO: Rename and change types of parameters
    private int customerId;

    private EditText editTextCustonmerId;
    private EditText editTextFirstName;
    private EditText editTextLastName;
    private EditText editTextAddress;
    private EditText editTextCity;
    private EditText editTextProvince;
    private EditText editTextPostalCode;
    private EditText editTextCountry;
    private EditText editTextHomePhone;
    private EditText editTextBusinessPhone;
    private EditText editTextEmail;
    private FloatingActionButton fABtnEdit;
    private FloatingActionButton fABtnSave;
    private FloatingActionButton fABtnCancel;

    //private OnFragmentInteractionListener mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param customerId CustomerId.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(int customerId) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putInt(CUSTOMER_ID, customerId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle("Customer Profile");


        if (getArguments() != null) {
            customerId = getArguments().getInt(CUSTOMER_ID);

            //Toast.makeText(this.getContext() , "The customer ID is " + customerId, Toast.LENGTH_LONG).show();

            customerDAO = new CustomerDAO();
            customerDAO.delegate = this;

            customerDAO.getCustomerById(customerId, getContext());

        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View myView = inflater.inflate(R.layout.fragment_profile, container, false);

        editTextCustonmerId = myView.findViewById(R.id.editTextCustonmerId);
        editTextFirstName = myView.findViewById(R.id.editTextFirstName);
        editTextLastName = myView.findViewById(R.id.editTextLastName);
        editTextAddress = myView.findViewById(R.id.editTextAddress);
        editTextCity = myView.findViewById(R.id.editTextCity);
        editTextProvince = myView.findViewById(R.id.editTextProvince);
        editTextPostalCode = myView.findViewById(R.id.editTextPostalCode);
        editTextCountry = myView.findViewById(R.id.editTextCountry);
        editTextHomePhone = myView.findViewById(R.id.editTextHomePhone);
        editTextBusinessPhone = myView.findViewById(R.id.editTextBusinessPhone);
        editTextEmail = myView.findViewById(R.id.editTextEmail);
        fABtnEdit = myView.findViewById(R.id.fABtnEdit);
        fABtnCancel = myView.findViewById(R.id.fABtnCancel);
        fABtnSave = myView.findViewById(R.id.fABtnSave);

        fABtnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editButtonClicked(); }});

        fABtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelButtonClicked(); }});

        fABtnSave.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                saveButtonClicked(); }});

        cancelButtonClicked();

        return myView;
    }

    @Override
    public void getCustomerProcessFinished(Customer customerOut) {
        customer = customerOut;
        displayCustomer(customer);
        //Toast.makeText(this.getContext(), customerOut.toString(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateCustomerProcessFinished(Customer updatedCustomerOut) {
        if(updatedCustomerOut != null)
        {
            displayCustomer(updatedCustomerOut);
            customer = updatedCustomerOut;
            Toast.makeText(getContext(), "Your information has been updated", Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(getContext(), "Something went wrong! Edits where not saved", Toast.LENGTH_LONG).show();
        }
    }

    //private display method
    private void displayCustomer(Customer customerToDisplay)
    {
        if(customerToDisplay != null)
        {
            editTextCustonmerId.setText(customerToDisplay.getCustomerId() + "");
            editTextFirstName.setText(customerToDisplay.getCustFirstName());
            editTextLastName.setText(customerToDisplay.getCustLastName());
            editTextAddress.setText(customerToDisplay.getCustAddress());
            editTextCity.setText(customerToDisplay.getCustCity());
            editTextProvince.setText(customerToDisplay.getCustProv());
            editTextPostalCode.setText(customerToDisplay.getCustPostal());
            editTextCountry.setText(customerToDisplay.getCustCountry());
            editTextHomePhone.setText(customerToDisplay.getCustHomePhone());
            editTextBusinessPhone.setText(customerToDisplay.getCustBusPhone());
            editTextEmail.setText(customerToDisplay.getCustEmail());
        }

    }

    private void editButtonClicked()
    {
        editTextAddress.setEnabled(true);
        editTextCity.setEnabled(true);
        editTextProvince.setEnabled(true);
        editTextPostalCode.setEnabled(true);
       // editTextCountry.setEnabled(true);
        editTextHomePhone.setEnabled(true);
        editTextBusinessPhone.setEnabled(true);
        editTextEmail.setEnabled(true);

        fABtnSave.setVisibility(View.VISIBLE);
        fABtnCancel.setVisibility(View.VISIBLE);
        fABtnEdit.setVisibility(View.GONE);
    }

    private void cancelButtonClicked()
    {

        //Diable the text fields
        editTextAddress.setEnabled(false);
        editTextCity.setEnabled(false);
        editTextProvince.setEnabled(false);
        editTextPostalCode.setEnabled(false);
        //editTextCountry.setEnabled(false);
        editTextHomePhone.setEnabled(false);
        editTextBusinessPhone.setEnabled(false);
        editTextEmail.setEnabled(false);

        //Disable any error messages
        editTextAddress.setError(null);
        editTextCity.setError(null);
        editTextProvince.setError(null);
        editTextPostalCode.setError(null);
        editTextHomePhone.setError(null);
        editTextBusinessPhone.setError(null);
        editTextEmail.setError(null);

        fABtnSave.setVisibility(View.GONE);
        fABtnCancel.setVisibility(View.GONE);
        fABtnEdit.setVisibility(View.VISIBLE);


        displayCustomer(customer);


    }

    private void saveButtonClicked()
    {
        boolean cancel = false;
        View focusView = null;

        String address = editTextAddress.getText().toString();
        //Check that address value is not empty
        if(TextUtils.isEmpty(address))
        {
            editTextAddress.setError("Address is a required field");
            focusView = editTextAddress;
            cancel = true;
        }

        String city = editTextCity.getText().toString();

        //Check that city value is not empty
        if(TextUtils.isEmpty(city))
        {
            editTextCity.setError("City is a required field");
            focusView = (focusView == null)? editTextCity : focusView;
            cancel = true;
        }
        String province = editTextProvince.getText().toString();
        //Check that province value is valid
        if(TextUtils.isEmpty(province))
        {
            editTextProvince.setError("Province is a required field");
            focusView = (focusView == null)? editTextProvince : focusView;
            cancel = true;
        }


        String postalCode = editTextPostalCode.getText().toString();

       //Validate the if the postal code is valid
        if(!InputValidator.isValidPostalCode(editTextPostalCode))
        {
            focusView = (focusView == null)? editTextProvince : focusView;
            cancel = true;
        }



        String homePhone = editTextHomePhone.getText().toString();
        if(TextUtils.isEmpty(homePhone))
        {
            editTextHomePhone.setError("Home phone is a required field");
            focusView = (focusView == null)? editTextHomePhone : focusView;
            cancel = true;
        }
        else if(!InputValidator.isValidPhoneNumber(editTextHomePhone))
        {
            focusView = (focusView == null)? editTextProvince : focusView;
            cancel = true;
        }

        String busPhone = editTextBusinessPhone.getText().toString();

        if(!TextUtils.isEmpty(busPhone) && !InputValidator.isValidPhoneNumber(editTextBusinessPhone))
        {
            focusView = (focusView == null)? editTextProvince : focusView;
            cancel = true;
        }

        String email = editTextEmail.getText().toString();

        if(!InputValidator.isValidEmail(editTextEmail))
        {
            focusView = (focusView == null)? editTextEmail : focusView;
            cancel = true;
        }

        if(cancel)
        {
            focusView.requestFocus();
        }
        else
        {
            Customer newCustomer = new Customer();
            //update the customer fields
            newCustomer.setCustomerId(customer.getCustomerId());
            newCustomer.setCustFirstName(customer.getCustFirstName());
            newCustomer.setCustLastName(customer.getCustLastName());
            newCustomer.setAgentId(customer.getAgentId());
            newCustomer.setCustAddress(address);
            newCustomer.setCustCity(city);
            newCustomer.setCustProv(province);
            newCustomer.setCustPostal(postalCode);
            newCustomer.setCustCountry(editTextCountry.getText().toString());
            newCustomer.setCustHomePhone(homePhone);
            newCustomer.setCustBusPhone(busPhone);
            newCustomer.setCustEmail(email);

            //Update the DB
            customerDAO.saveCustomer(newCustomer, getContext());

            //Disable editing on UI
            cancelButtonClicked();
        }


    }


}
