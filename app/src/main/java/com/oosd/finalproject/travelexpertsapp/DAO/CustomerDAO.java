package com.oosd.finalproject.travelexpertsapp.DAO;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.oosd.finalproject.travelexpertsapp.api.APIUtils;
import com.oosd.finalproject.travelexpertsapp.model.Customer;

import org.json.JSONException;
import org.json.JSONObject;


public class CustomerDAO {

    //private static final String CUSTOMER_SERVICE_URL = "http://localhost:8080/TravelExperts/webapi/customers/";
    private static final String CUSTOMER_SERVICE_URL = APIUtils.BASE_URL + "customers/";
    //private static String getCustomerURL = "";
    private static String UPDATE_CUSTOMER_URL = "update";
    StringBuffer buffer = new StringBuffer();
    public CustomerAsyncResonponse delegate;


    public void getCustomerById(int customerId, Context context)
    {
        String getCustomerURL = CUSTOMER_SERVICE_URL + customerId;

        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, getCustomerURL,
                null,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        Gson gson = new Gson();
                        Customer customer = gson.fromJson(response.toString(), Customer.class);
                        delegate.getCustomerProcessFinished(customer);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {

                    }
                });
        requestQueue.add(jsonObjectRequest);
    }

    public void saveCustomer(Customer customer, Context context)
    {
        String updateCustomerURL = CUSTOMER_SERVICE_URL + UPDATE_CUSTOMER_URL;

        Gson gson = new Gson();
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = new JSONObject(gson.toJson(customer, Customer.class));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.PUT, updateCustomerURL,
                jsonObject,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response)
                    {
                        Gson gson = new Gson();
                        Customer customer = gson.fromJson(response.toString(), Customer.class);
                        delegate.updateCustomerProcessFinished(customer);
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {

                    }
                });
        requestQueue.add(jsonObjectRequest);
    }



}